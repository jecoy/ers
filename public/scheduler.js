
    $user_id = {{Session::get('user_id')}};
    var status = "";
    var isUpdating = false;
        $(document).ready(function() {  
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        

        function fetch(){
            var eq_list = new kendo.data.DataSource({
            transport: {
                read: {
                url: "/reservation/equipment_list",
                dataType: "json",
                contentType: "application/json"
                }
            }
            });
            $("#equipments").kendoMultiSelect({
                dataSource: eq_list,
                dataTextField: "equipment_name",
                dataValueField: "id",
                filter: "contains",
            });

            var room_list = new kendo.data.DataSource({
            transport: {
                read: {
                url: "/reservation/rooms_list",
                dataType: "json"
                }
            }
            });
            $("#rooms").kendoDropDownList({
            dataSource: room_list,
            dataTextField: "room_name",
            dataValueField: "id",
            filter: "contains",
            });
            $('span.k-select').click(function(){
                $('a.k-link.k-nav-prev').hide();
                $('a.k-link.k-nav-next').hide();
                // $('a.k-link.k-nav-fast').hide();
                //console.log("POTETAM")
            });
        };
        function scheduler_remove(e) {
            if($user_id!=e.event.user_id){
                e.preventDefault();
            }
        }
        function hide(e){
            var buttonsContainer = e.container.find(".k-edit-buttons");
            var updateButton = buttonsContainer.find(".k-scheduler-update");
            var deleteButton = buttonsContainer.find(".k-scheduler-delete");
            var cancelButton = buttonsContainer.find(".k-scheduler-cancel");
            updateButton.hide();
            deleteButton.hide();
            cancelButton.hide();
        }
        function dataBound(){
            if(status=="saved"){
            status = "";
            }
            else if(status=="updated"){
                console.log("Updated");
                status = "";
                isUpdating = false;
            }
            else{
                console.log("Welcome !");
            }
        };

        var model = kendo.observable({

            equipments : new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/reservation/equipment_list",
                    dataType: "json"
                }
            }
            }),
            rooms : new kendo.data.DataSource({
                transport: {
                    read: {
                    url: "/reservation/rooms_list",
                    dataType: "json"
                    }
                }
            }),
            reserves : new kendo.data.SchedulerDataSource({
                transport: {
                    read: {
                        url:"/reservation/scheduler_list",
                        dataType:"json"
                    },
                    update: {
                        url: "/reservation/update",
                        dataType: "json",
                        type: "POST",
                        complete: function(jqXhr, textStatus) {
                            if (textStatus == 'success') {
                                console.log('Success');
                            } else {
                                alert('Unable to update due to equipment shortage!');
                            }
                        }
                    },
                    create: {
                        url: "/reservation/store",
                        dataType: "json",
                        type:"POST",
                        complete: function(jqXhr, textStatus) {
                            if (textStatus == 'success') {
                                location.reload(true);
                            } 
                            else {                               
                                window.location = 'conflict';
                            }
                        }
                    },
                    destroy: {
                        url: "/reservation/destroy",
                        dataType: "json",
                        type:"POST"
                    },
                        parameterMap: function(options, operation) {
                            if(operation!='read'){
                                if(operation=='create'){
                                    var starts = new Date(options.start);
                                    options.start = kendo.toString(new Date(starts), "yyyy-MM-dd HH:mm:sszz");
                                    var ends = new Date(options.end);
                                    options.end = kendo.toString(new Date(ends), "yyyy-MM-dd HH:mm:sszz");
                                    options.user_id = $user_id;
                                    options.IsAllDay = false;
                                    options.ReservationStatus = "Reserved"; 
                                }   
                                else{
                                    var starts = new Date(options.start);
                                    options.start = kendo.toString(new Date(starts), "yyyy-MM-dd HH:mm:sszz");
                                    var ends = new Date(options.end);
                                    options.end = kendo.toString(new Date(ends), "yyyy-MM-dd HH:mm:sszz");
                                    options.user_id = $user_id; 
                                    options.ReservationStatus = "Reserved"; 
                                }
                            //console.log(options);
                            return options;
                            }
                        }
                    },
                schema: {
                    model: {
                        id: "id",
                        fields:{
                            title: { from: "Title", defaultValue: "Equipment Reservation", validation: { required: true }},
                            start: { type: "date"},
                            end: { type: "date"},
                            startTimezone: { from: "StartTimezone" },
                            endTimezone: { from: "EndTimezone" },
                            description: { from: "Purpose"},
                            recurrenceId: { from: "RecurrenceID" },
                            recurrenceRule: { from: "RecurrenceRule" },
                            recurrenceException: { from: "RecurrenceException" },
                            roomId: { from: "RoomID", nullable: true },
                            equipments: { from: "Equipments", nullable: true },
                            user_id: {type : "number"},
                            isAllDay: { type: "boolean", from: "IsAllDay"}
                        }
                    }
                }
        }),
        init : function(e){
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;
        $("#scheduler").kendoScheduler({
        date: new Date(date),
        remove:scheduler_remove,
        edit: function(e) {
            if(e.event.id!=0){      
                if(e.event.user_id != $user_id){
                    hide(e);
                }     
                else{
                    isUpdating = true;   
                    console.log("Editing", e.event.id);
                }
            }
            fetch();
           // e.container.find("input:first").remove();
           // e.container.find("label:first").remove();
        },  
        editable: {
            template: $("#customEditorTemplate").html(),
        },
        dataBound: function(e) { 
           dataBound();
        },
        save: function(e) { 
            if(isUpdating){
                status="updated";
            }
            else{
                status = "saved";
            }
        },  
        eventTemplate: $("#event-template").html(),
        startTime: new Date(dateTime),
        dataSource: this.reserves,
        height: 800,
        views: [
            { type: "month", 
            selected: true },
            "day"
        ],
        timezone: "Etc/UTC",
        resources: [
                {
                    field: "roomId",
                    name: "Rooms",
                    dataSource: this.rooms,
                    title: "Room"
                },
                {
                    field: "equipments",
                    name: "Equipments",
                    dataSource: this.equipments,
                    multiple: true,
                    title: "Equipments"
                }
            ]
        });

        },
        
        });
        kendo.bind($("#whole"),model);
        model.init();
    }); 