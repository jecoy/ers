@extends('layouts.firstheader')
 
@section('content')
    @include('layouts.header')
    <div id="whole">
        <div id="grid" class="fadeIn fourth" data-csrf="{!!csrf_token()!!}">
        </div>
    </div>
    <script>
    var dates_id = '{{Session::get('dates_id')}}';
    var urlName,schemaName,titleName;
    if(dates_id=='return_date'){
        assignText("/return_date_list","return_date","Return Date");
    }

    else if(dates_id=='borrow_date'){
        assignText("/borrow_date_list","borrow_date","Borrow Date");
    }

    function assignText(url,schema,title){
        urlName = url;
        schemaName = schema; 
        titleName = title;
    }

        $(document).ready(function(){
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
            });

            var data = new kendo.data.DataSource({
            transport:{
                read:{
                    url:urlName,
                    dataType:"json"
                }
            },
            schema:{
                model:{
                    id:"id",
                    fields:{
                        reservation_id:{
                            type:"number"
                        },
                        Purpose:
                        {
                            type:"string",
                            editable:false,
                            validation:{required:true}
                        },
                        start:
                        {
                            type:"date",
                            editable:false,
                            validation:{required:true}
                        },
                        end:
                        {
                            editable:false,
                            type:"date",
                            validation:{required:true}
                        },
                        schemaName:{
                            type:"date"
                        }
                    }
                }
            },
            pageSize:10
        });

        $("#grid").kendoGrid({
                dataSource: data,
                selectable: true,
                height:600,
                editable: "popup",
                filterable: true,
                sortable: {
                            mode: "multiple",
                            allowUnsort: true,
                            showIndexes: true
                        },
                toolbar: ["search"],
                columns: [
                            { 
                            field: "reservation_id",
                            title:"Reservation ID"
                            },
                            { 
                                field: "Purpose",
                                title:"Purpose" 
                            },
                            { 
                                title:"Room", 
                                template:"#=roomTemplate(data)#"
                            },
                            { 
                                title:"Equipments",
                                template:"#=equipmentsTemplate(data)#"
                            }, 
                            { 
                                field: "start",
                                title:"Start",
                                //template: '#= kendo.toString(kendo.parseDate(start), "MM/dd/yyyy HH:mm:ss")#' 
                                format: "{0:MMM dd,yyyy hh:mm tt}",
                                parseFormats: ["yyyy-MM-dd'T'HH:mm.zz"]
                            },
                            { 
                                field: "end",
                                title:"End",
                                //template: '#= kendo.toString(kendo.parseDate(end), "MM/dd/yyyy HH:mm:ss")#'
                                format: "{0:MMM dd,yyyy hh:mm tt}",
                                parseFormats: ["yyyy-MM-dd'T'HH:mm.zz"] 
                            },
                            { 
                                field: schemaName,
                                title: titleName,
                                format: "{0:MMM dd,yyyy hh:mm tt}",
                                parseFormats: ["yyyy-MM-dd'T'HH:mm.zz"] 
                            }
                        ],
                pageable:{
                    pageSize:10,
                    refresh:true,
                    buttonCount:5,
                    messages:{
                        display:"{0}-{1}of{2}"
                    }
                }
            });
        });
        
            function equipmentsTemplate(data){ 
            var template = kendo.template($('#equipmentsTemplate').html()); 
            var result = template(data); 
            return result;
            }

            function roomTemplate(data){ 
                var template = kendo.template($('#roomTemplate').html()); 
                var result = template(data); 
                return result;
            }    
</script>

<script id="equipmentsTemplate" type="text/x-kendo-template">
    <div>    
        <ul>
            # for (var i = 0; i < data.Equipments.length; i++) { #
                <li>#=data.Equipments[i].equipment_name#</li>
            # } #
        </ul>
    </div>
</script>
<script id="roomTemplate" type="text/x-kendo-template">
    <div>    
        #=data.RoomID.room_name#
    </div>
</script>
@endsection