@extends('layouts.firstheader') 
@section('content') 
@include('layouts.header')
    <div id="grid" class="fadeIn fourth" data-csrf="{!!csrf_token()!!}">
    </div>

<div id="myModal" class="modal">
    <div class="modal-content">
    
    <div class="modal-header">
            <h2>FIXED ASSET</h2>
        </div>
        <div class="modal-body" style="overflow:scroll; height:500px;">          
            <input id="assets" style="width: 100%;" />
            <button class="btn btn-success" id="add" style="margin-top:3px; margin-bottom:3px;">ADD</button> 
            <table class="table table-hover table-bordered" id="tableFixAsset"> 
            </table>
        </div> 
        <div class="modal-header">
            <h2>REMARKS</h2>
        </div>
        <div class="modal-body">
        <textarea class="form-control" id="remarks" rows="10"></textarea>
        </div>
        <div class="modal-footer">
            <button class="confirm" id="confirm">CONFIRM</button>
            <button class="cancel" id="cancel">CANCEL</button>
        </div> 
    </div>  
</div>

<script>
    var id = {{Session::get('id')}};
    var buttonOne="";
    var buttonTwoClass=""
    var buttonTwo="";
    var buttonThree="";
    var buttonThreeClass="";

    if(id==1){
        assignText("Borrowed","Cancel","Cancel");
    }
    else if(id==2){
        assignText("Returned","Cancel","Cancel");
    }
    else if(id==3){
        assignText("Remarks","Cancel","Cancel");
    }
    else if(id==4){
        assignText("Confirmed","Cancel","Cancel");
    }
    else{
        alert('ERROR');
    }

    function assignText(buttonOneText, buttonTwoText, buttonTwoClassName){
        buttonOne = buttonOneText;
        buttonTwo = buttonTwoText;
        buttonTwoClass = buttonTwoClassName;
    }

        $(document).ready(function() { 
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;
        $('div.modal').hide(); 
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

            var data = new kendo.data.DataSource({
                transport:{
                            read: { 
                            url:"/reservation/list",
                            dataType:"json"
                        },
                        update: {
                            url: "/reservation/update",
                            dataType: "json",
                            type: "POST"
                        },
                        create: {
                            url: "/reservation/store",
                            dataType: "json",
                            type:"POST"
                        },
                        destroy: {
                            url: "/reservation/destroy",
                            dataType: "json",
                            type:"POST"                          
                        },
                        parameterMap: function(options, operation) {
                            if(operation!='read'){
                                if(operation=='create'){
                                    var starts = new Date(options.start);
                                    options.start = kendo.toString(new Date(starts), "yyyy-MM-dd HH:mm:ss");
                                    var ends = new Date(options.end);
                                    options.end = kendo.toString(new Date(ends), "yyyy-MM-dd HH:mm:ss"); 
                                    options.status = "reserved";
                                }
                                else{
                                    var starts = new Date(options.start);
                                    options.start = kendo.toString(new Date(starts), "yyyy-MM-dd HH:mm:ss");
                                    var ends = new Date(options.end);
                                    options.end = kendo.toString(new Date(ends), "yyyy-MM-dd HH:mm:ss"); 
                                    options.status = "updated";
                                }
                            console.log(options);
                            return options;
                            }
                        }
                        },
                        schema:{
                            model:{
                                id:"id",
                                fields:{
                                    id:{
                                        editable:false
                                    },
                                    fullname:
                                    {
                                        type:"string",
                                        editable:false,
                                        validation:{required:true}
                                    },
                                    department:
                                    {
                                        type:"string",
                                        editable:false,
                                        validation:{required:true}
                                    },
                                    Purpose:
                                    {
                                        type:"string",
                                        editable:false,
                                        validation:{required:true}
                                    },
                                    start:
                                    {
                                        type:"date",
                                        editable:false,
                                        validation:{required:true}
                                    },
                                    end:
                                    {
                                        editable:false,
                                        type:"date",
                                        validation:{required:true}
                                    },
                                }
                            }
                        },
                        pageSize:10
        });

            $("#grid").kendoGrid({
                dataSource: data,
                selectable: true,
                height:600,
                editable: "popup",
                filterable: true,
                dataBound: onDataBound,
                sortable: {
                            mode: "multiple",
                            allowUnsort: true,
                            showIndexes: true
                        },
                toolbar: ["search","excel"],
                pdf: {
                    allPages: true,
                    avoidLinks: true,
                    paperSize: "A4",
                    margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
                    landscape: true,
                    repeatHeaders: true,
                    template: $("#page-template").html(),
                    scale: 0.8,
                },
                excel: {
                fileName: "reservations.xlsx"
                },
                columns: [
                    { 
                        field: "id",
                        title:"Reservation ID"
                    },
                    { 
                        field: "fullname",
                        title:"Name" 
                    },
                    { 
                        field: "department",
                        title:"Department" 
                    },
                    { 
                        field: "Purpose",
                        title:"Purpose" 
                    },
                    { 
                        title:"Room", 
                        template:"#=roomTemplate(data)#"
                    },
                    { 
                        title:"Equipments",
                        template:"#=equipmentsTemplate(data)#"
                    }, 
                    { 
                        field: "start",
                        title:"Start",
                        //template: '#= kendo.toString(kendo.parseDate(start), "MM/dd/yyyy HH:mm:ss")#' 
                        format: "{0:MMM dd,yyyy hh:mm tt}",
                        parseFormats: ["yyyy-MM-dd'T'HH:mm.zz"]
                    },
                    { 
                        field: "end",
                        title:"End",
                        //template: '#= kendo.toString(kendo.parseDate(end), "MM/dd/yyyy HH:mm:ss")#'
                        format: "{0:MMM dd,yyyy hh:mm tt}",
                        parseFormats: ["yyyy-MM-dd'T'HH:mm.zz"] 
                    },
                    { 
                        command: [
                        {
                            name: buttonOne,
                            text: buttonOne,
                            className: buttonOne
                        },/*
                        {
                            name: buttonThreeClass,
                            text: buttonThree,
                            className: buttonThreeClass
                        },*/
                        {
                            name: buttonTwoClass,
                            text: buttonTwo,
                            className: buttonTwoClass
                        }                        
                    ],
                        title: "&nbsp;", 
                        width: "150px" 
                    }
                    ],
                    pageable:{
                    pageSize:10,
                    refresh:true,
                    buttonCount:5,
                    messages:{
                        display:"{0}-{1}of{2}"
                    }
                }
            }); 

            dropdown(1);
            

        });
         
        var modal = document.getElementById("myModal");      
        var modal2 = document.getElementById("dropdownModal");      
        var rid, remarks, grid, myVar, fxa_id, url;

        function dropdown(session){
            var asset_list = new kendo.data.DataSource({
                transport: {
                    read: {
                    url: "/reservation/assets_list",
                    dataType: "json"
                    }
                }
            });
                
            $("#assets").kendoDropDownList({
            dataSource: asset_list,
            dataTextField: "desc",
            dataValueField: "id",
            filter: "contains",
            })
            .closest(".k-widget")
            .attr("id", "products_wrapper");
            
             var dropdownlist = $("#assets").data("kendoDropDownList"),
                setValue = function(e) {
                    if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode)
                        dropdownlist.value($("#value").val());
                },
                setIndex = function(e) {
                    if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode) {
                        var index = parseInt($("#index").val());
                        dropdownlist.select(index);
                    }
                },
                setSearch = function(e) {
                    if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode)
                        dropdownlist.search($("#word").val());
                };
                
            $("#add").click(function() { 
                fxa_id = dropdownlist.value(); 
                if(session!=2){
                    addBorrowedFixAsset();
                }
            });
        }

        function equipmentsTemplate(data){ 
            var template = kendo.template($('#equipmentsTemplate').html()); 
            var result = template(data); 
            return result;
        }

        function roomTemplate(data){ 
            var template = kendo.template($('#roomTemplate').html()); 
            var result = template(data); 
            return result;
        }

        function onDataBound(arg) {     
            $('a.k-button.k-button-icontext.Null.k-grid-Null').hide();

            if('{{Session::get('user_role')}}'!='Admin'){
                $('a.k-button.k-button-icontext.Borrowed.k-grid-Borrowed').remove();
                $('a.k-button.k-button-icontext.Cancel.k-grid-Cancel').remove();
                $('a.k-button.k-button-icontext.Asset.k-grid-Asset').remove();
                $('a.k-button.k-button-icontext.Remarks.k-grid-Remarks').remove();
               // $('a.k-button.k-button-icontext.Returned.k-grid-Returned').hide();
            }
        }

        $("#grid").delegate(".Borrowed", "click", function(e) {
            // Get the modal                           
            grid = $("#grid").data("kendoGrid");
            myVar = grid.dataItem($(this).closest("tr"));  
            rid = myVar.id;             
            modal.style.display = "block";  
            populateFxa();
            // When the user clicks anywhere outside of the modal, close it
            /*window.onclick = function(event) {
                if (event.target == modal) {
                    $("#remarks").val("");     
                    modal.style.display = "none";
                }
            }*/
        });

        $("#grid").delegate(".Returned", "click", function(e) {
            grid = $("#grid").data("kendoGrid");
            myVar = grid.dataItem($(this).closest("tr"));
            rid = myVar.id;
            remarks="";
            if('{{Session::get('user_role')}}'=='Admin'){
            populateFxa();
            openModal(rid);
            }
            else{
                var r = confirm("Please confirm that this is already "+buttonOne+".");
                if (r == true) {   
                    grid = $("#grid").data("kendoGrid"); 
                    $.ajax({
                        url: "/change_status/{rid,remarks}",
                        type: 'POST',
                        data: 
                        { 
                            rid: rid,
                            remarks:remarks 
                        },
                        success: function(response)
                        {
                            $("#remarks").val("");      
                            modal.style.display = "none";                
                            grid.dataSource.read();
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
                            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                        } 
                    });
                }
            }
        });

        $("#grid").delegate(".Confirmed", "click", function(e) {
            grid = $("#grid").data("kendoGrid");
            myVar = grid.dataItem($(this).closest("tr"));
            rid = myVar.id;
            populateFxa();
            openModal(rid);
        });

        $("#grid").delegate(".Asset", "click", function(e) {
            grid = $("#grid").data("kendoGrid");
            myVar = grid.dataItem($(this).closest("tr"));
            rid = myVar.id;
            modal2.style.display = "block";
            populateFxa();
        });


        $("#grid").delegate(".Remarks", "click", function(e) {
            modal.style.display = "block";  
            grid = $("#grid").data("kendoGrid");
            myVar = grid.dataItem($(this).closest("tr"));  
            rid = myVar.id;
            populateFxa();
            $.ajax({
                    url: "/getRemarks/{rid}",
                    type: 'POST',
                    data: 
                    { 
                        rid: rid 
                    },
                    success: function(response)
                    {
                        $("#remarks").val(response);      
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                    } 
                })
        });

        $("#grid").delegate(".Cancel", "click", function(e) {
            grid = $("#grid").data("kendoGrid");
            myVar = grid.dataItem($(this).closest("tr"));
            rid = myVar.id; 
            var r = confirm("Are you sure you want to cancel ?");
            if (r == true) {                
                $.ajax({
                    url: "/cancel_status/{rid}",
                    type: 'POST',
                    data: 
                    { 
                        rid: rid 
                    },
                    success: function(response)
                    {
                        grid.dataSource.read();
                        console.log("success");
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert("Unable to cancel, please cancel attached fixed asset/s first."); 
                    } 
                });
            } 
            else{                
                return;
            }
        });

        

        $("#confirm").click(function(){
            grid = $("#grid").data("kendoGrid");
            remarks = $.trim($("#remarks").val());  
            //alert(rid+" - "+remarks);
            if(remarks!=""){
                $.ajax({
                    url: "/change_status/{rid,remarks}",
                    type: 'POST',
                    data: 
                    { 
                        rid: rid,
                        remarks:remarks 
                    },
                    success: function(response)
                    {
                        $("#remarks").val("");      
                        modal.style.display = "none";                
                        grid.dataSource.read();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert('The fixed assets are not returned yet.');
                    } 
                });
            }
            else{
                alert("Please enter remarks");
            }
        });
        
        $("#cancel").click(function() {  
            $('div.modal').hide(); 
            $("#remarks").val("");          
        });
        $("#cancelDropDown").click(function() {  
            $('#dropdownModal').hide(); 
            $("#remarks").val("");          
        });

        function openModal(rid){
            modal.style.display = "block";  
            console.log(rid);
            $.ajax({
                    url: "/getRemarks/{rid}",
                    type: 'POST',
                    data: 
                    { 
                        rid: rid 
                    },
                    success: function(response)
                    {
                        $("#remarks").val(response);      
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                    } 
                })
        }

        function populateFxa(){    
            $.ajax({
                    url: "/getAssets/{rid}",
                    type: 'POST',
                    data: 
                    { 
                        rid: rid 
                    },
                    success: function(response)
                    {
                        $('#tableFixAsset').html(response);                
                        $("button.btn.btn-danger").click(function(event) { 
                            borrowedFxa_text = event.currentTarget.innerText;
                            borrowedFxa_id = event.currentTarget.value;
                            /*if(borrowedFxa_text=='CANCEL'){
                                url = "/cancelAsset/{borrowedFxa_id}";
                            }
                            else{
                                url = "/returnAsset/{borrowedFxa_id}";
                            }*/
                            $.ajax({
                                url: "/cancelAsset/{borrowedFxa_id}",
                                type: 'POST',
                                data: 
                                { 
                                    borrowedFxa_id: borrowedFxa_id 
                                },
                                success: function(response)
                                {
                                    populateFxa();
                                    dropdown(2);
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                                    //alert("Status: " + textStatus);
                                    //alert("Error: " + errorThrown); 
                                } 
                            });  
                        });
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                    } 
                });  
        }

        function addBorrowedFixAsset(){
            $.ajax({
                    url: "/borrowAsset/{rid,fxa_id}",
                    type: 'POST',
                    data: 
                    { 
                        rid: rid ,
                        fxa_id: fxa_id
                    },
                    success: function(response)
                    {
                         populateFxa();
                         dropdown(2);
                         if(response=='404'){
                            alert("The Fix Asset is borrowed already.");
                         }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        //alert("The Fix Asset is borrowed already.");
                    } 
                });  
        }
</script>
<script type="x/kendo-template" id="page-template">
      <div class="page-template">
        <div class="header">
          <div style="float: right">Page #: pageNum # of #: totalPages #</div>
        </div>
        <div class="footer">
          Page #: pageNum # of #: totalPages #
        </div>
      </div>
</script>
<script id="equipmentsTemplate" type="text/x-kendo-template">
    <div>    
        <ul>
            # for (var i = 0; i < data.Equipments.length; i++) { #
                <li>#=data.Equipments[i].equipment_name#</li>
            # } #
        </ul>
    </div>
</script>
<script id="roomTemplate" type="text/x-kendo-template">
    <div>    
        #=data.RoomID.room_name#
    </div>
</script>
<style>
        /* Page Template for the exported PDF */
        .page-template {
          font-family: "DejaVu Sans", "Arial", sans-serif;
          position: absolute;
          width: 100%;
          height: 100%;
          top: 0;
          left: 0;
        }
        .page-template .header {
          position: absolute;
          top: 30px;
          left: 30px;
          right: 30px;
          border-bottom: 1px solid #888;
          color: #888;
        }
        .page-template .footer {
          position: absolute;
          bottom: 30px;
          left: 30px;
          right: 30px;
          border-top: 1px solid #888;
          text-align: center;
          color: #888;
        }
</style>
@endsection