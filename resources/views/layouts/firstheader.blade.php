<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('bootstrap.css')}}" rel="stylesheet">
    <link href="{{ asset('animation.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('styles/kendo.default.mobile.min.css')}}" />
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/kendo.all.min.js')}}"></script>
    <script src="{{ asset('pako_deflate.min.js')}}"></script>
    <script src="{{ asset('jszip.min.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('styles/kendo.common.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('styles/kendo.default.min.css')}}" />

    <link href="{{ asset('fontawesome/css/all.css')}}" rel="stylesheet">
    <script src="{{ asset('fontawesome/js/all.js')}}"></script>
    <link href="{{ asset('fontawesome/css/fontawesome.css')}}" rel="stylesheet">
    <link href="{{ asset('fontawesome/css/brands.css')}}" rel="stylesheet">
    <link href="{{ asset('fontawesome/css/solid.css')}}" rel="stylesheet">
    <link rel="icon" href="{{asset('/fav.png')}}" type="image/gif">
<style>
.responsive {
  max-width: 100%;
  height: auto;
}

body{
    background-image: linear-gradient(#89CFF0,#56baed, #007bff);
    background-repeat: no-repeat;
    font-family: "DejaVu Sans", sans-serif;
    font-weight: 400;
    font-variant: small-caps;
    /*background-image: linear-gradient(#89CFF0,#56baed, #007bff);
    background-repeat: no-repeat; 
    background-image: url({{ asset('bg.png') }});*/
}
img{
    padding:20px;
}
.container{
    background-image: linear-gradient(#89CFF0,#56baed, #007bff);
    padding:10px;
}
.navbar-brand:hover{
    padding:3px;
    background-color: #89CFF0;
    border-radius: 5px;    
}

.navbar-dark{
    margin-bottom:10px;
    border-radius: 25px;
}

</style>

<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "navbar navbar-dark bg-primary fadeIn second topnav") {
    x.className += " responsive";
  } else {
    x.className = "navbar navbar-dark bg-primary fadeIn second topnav";
  }
}
</script>
</head>

<body>
<div style="padding:20px;">
    <div class="jumbobox container fadeIn first">
        <div class="navbar navbar-dark bg-primary fadeIn second">
            <a class="fadeIn third" href="{{ route('index') }}">
                <img src="{{asset('logo.png')}}" alt="Italian Trulli" class="responsive fadeIn third">
            </a>
        </div>
        
        <nav class="navbar navbar-dark bg-primary fadeIn second topnav" id="myTopnav">
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                <i class="fa fa-bars"></i>
            </a>
            @if(Session::get('user_role')=='Admin'||Session::get('user_role')=='User')  
            <a class="navbar-brand fadeIn third" href="{{ route('index') }}"><i class="fas fa-home"></i>Home</a>
            <a class="navbar-brand fadeIn third" href="{{ route('reservation_index', ['id' => 1]) }}"><i class="far fa-calendar-alt"></i>Reservations</a>
            @if(Session::get('user_role')=='Admin')  
            <a class="navbar-brand fadeIn third" href="{{ route('room_index') }}"><i class="fas fa-door-closed"></i>Rooms</a>
            <a class="navbar-brand fadeIn third" href="{{ route('equipment_index') }}"><i class="fas fa-laptop"></i>Equipment</a>
            <a class="navbar-brand fadeIn third" href="{{ route('users_index') }}"><i class="fas fa-users"></i>Users</a>
            <a class="navbar-brand fadeIn third" href="{{ route('asset_index') }}"><i class="fas fa-laptop"></i>Assets</a>
            @endif
            @endif
            <a class="navbar-brand fadeIn third" href="{{ route('logout') }}" style="float:right;"><i class="fas fa-sign-out-alt"></i>Logout</a>
        </nav>
        <div class="fadeInDown">
            @yield('content')
        </div>
    </div>
</div>
<!--
    <span style="color:white; font-size: 50%; background-color:red; padding:2px; margin:3px;">1</span>
-->
</body>
</html>