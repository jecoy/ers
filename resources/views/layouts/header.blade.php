        <nav class="navbar navbar-dark bg-primary fadeIn second topnav" id="mySecondnav">
            <a href="javascript:void(0);" class="icon" onclick="mySecondNavFunction()">
                <i class="fa fa-bars"></i>
            </a>
            @if(Session::get('user_role')=='Admin'||Session::get('user_role')=='User')
            <a class="navbar-brand fadeIn third" href="{{ route('reservation_index', ['id' => 1]) }}">Reserved</a>
            <a class="navbar-brand fadeIn third" href="{{ route('reservation_index', ['id' => 2]) }}">Borrowed</a>
            <a class="navbar-brand fadeIn third" href="{{ route('reservation_index', ['id' => 3]) }}">Returned</a>
            @if(Session::get('user_role')=='Admin')
            <a class="navbar-brand fadeIn third" href="{{ route('reservation_index', ['id' => 4]) }}">Pending</a>
            @endif        
            <a class="navbar-brand fadeIn third" href="{{ route('dates', ['id' => 'return_date']) }}" style="float:right;">Return Dates</a>
            <a class="navbar-brand fadeIn third" href="{{ route('dates', ['id' => 'borrow_date']) }}" style="float:right;">Borrow Dates</a>
            @endif   
        </nav>
        <script>
            function mySecondNavFunction() {
            var x = document.getElementById("mySecondnav");
                if (x.className === "navbar navbar-dark bg-primary fadeIn second topnav") {
                    x.className += " responsive";
                } else {
                    x.className = "navbar navbar-dark bg-primary fadeIn second topnav";
                }
            }
        </script>