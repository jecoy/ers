@extends('layouts.firstheader')
 
@section('content')
<div class="jumbotron" style="background-color:white;">
    @if(Session::has('message'))
        <div class="message" style="padding: 10px; color:red;">
        @foreach (Session::get('message') as $messages)
           {{$messages}}</br>
        @endforeach
        </div>
    @endif
</div>    
@endsection