
@extends('layouts.firstheader')
@section('content')
    <div id="whole">
        <div id="scheduler" class="navbar navbar-dark bg-primary" data-csrf="{!!csrf_token()!!}">
        </div>
    </div>
    <script id="event-template" type="text/x-kendo-template">
        <div>
            <p>#: fullname #</p>
            <p>#: department #</p>
        </div>
    </script>
    <script id="customEditorTemplate" type="text/x-kendo-template">
    <div class="k-edit-label" id="fullnamelbl"><label for="fullname">Fullname</label></div>
    <div data-container-for="fullname" class="k-edit-field" id="fullname">
        <input type="text" name="Fullname" class="k-textbox" data-bind="value:fullname" readonly/>        
    </div>
    <div class="k-edit-label"  id="departmentlbl"><label for="department">Department</label></div>
    <div data-container-for="department" class="k-edit-field" id="department">
        <input type="text" name="Department" class="k-textbox" data-bind="value:department" readonly/>        
    </div>

    <div class="k-edit-label">
        <label for="start">Start</label>
    </div>
    <div data-container-for="start" class="k-edit-field">
        <input type="text"
               data-role="datetimepicker"
               data-interval="10"
               data-type="date"
               data-bind="value:start,invisible:isAllDay"
               name="start" read />
        <input type="text" 
               data-type="date" 
               data-role="datetimepicker" 
               data-bind="value:start,visible:isAllDay" 
               name="start"
               data-interval="10"
                />
        <span data-bind="text: startTimezone">
        </span>
        <span data-for="start" class="k-invalid-msg" style="display: none;">
        </span>
    </div>

    <div class="k-edit-label">
        <label for="end">
          End
        </label>
    </div>
    <div data-container-for="end" class="k-edit-field">
        <input type="text" 
               data-type="date" 
               data-role="datetimepicker" 
               data-bind="value:end,invisible:isAllDay" 
               data-interval="10"
               name="end" data-datecompare-msg="End date should be greater than or equal to the start date" />
        <input type="text" 
               data-type="date" 
               data-role="datetimepicker" 
               data-bind="value:end,visible:isAllDay" 
               name="end" 
               data-interval="10"
               data-datecompare-msg="End date should be greater than or equal to the start date" />
        <span data-bind="text: endTimezone">
        </span>
        <span data-bind="text: startTimezone, invisible: endTimezone">
        </span>
        <span data-for="end" class="k-invalid-msg" style="display: none;">
        </span>
    </div>

    <!--

    <div class="k-edit-label"><label for="isAllDay">All day event</label></div>
    <div data-container-for="isAllDay" class="k-edit-field">
        <input type="checkbox" 
               name="isAllDay" 
               data-type="boolean" 
               data-bind="checked:isAllDay">
    </div>
    <div class="k-edit-label">
        <label for="recurrenceRule">
            Repeat
        </label>
    </div>

    <div data-container-for="recurrenceRule" class="k-edit-field">
        <div data-bind="value:recurrenceRule" 
             name="recurrenceRule" 
             data-role="recurrenceeditor">
        </div>
    </div>

    -->

    <div class="k-edit-label"><label for="description">Purpose</label></div>
    <div data-container-for="description" class="k-edit-field">
        <textarea name="Purpose" class="k-textbox" data-bind="value:description" required>
        </textarea>
    </div>

    <div class="k-edit-label">
        <label for="roomId">
            Room
        </label>
    </div>

    <div data-container-for="roomId" class="k-edit-field">
        <span title="Equipment Reservation" class="k-widget k-dropdown" unselectable="on" role="listbox">
            <span unselectable="on" class="k-dropdown-wrap k-state-default">
                <span unselectable="on" class="k-input">
                </span>
                    <span unselectable="on" class="k-select" aria-label="select">
                        <span class="k-icon k-i-arrow-60-down">
                        </span>
                    </span> 
            </span>
                    <select id="rooms" data-bind="value:roomId" style="display: none;" name="Rooms" required>
                     </select>
        </span>
    </div>

    <div class="k-edit-label">
        <label for="equipments">Equipments</label>
    </div>
    <div data-container-for="equipments" class="k-edit-field">
        <div class="k-widget k-multiselect k-multiselect-clearable" unselectable="on" title="" style="">
            <select id="equipments" data-bind="value:equipments" name="Equipments" required>
            </select>
        </div>
    </div>
    </script>
    
    <script>
    $user_id = {{Session::get('user_id')}};
    $user_role = '{{Session::get('user_role')}}';
    var status = "";
    var isUpdating = false;
        $(document).ready(function() {  
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        

        function fetch(){
            var eq_list = new kendo.data.DataSource({
            transport: {
                read: {
                url: "/reservation/equipment_list",
                dataType: "json",
                contentType: "application/json"
                }
            }
            });
            $("#equipments").kendoMultiSelect({
                dataSource: eq_list,
                dataTextField: "equipment_name",
                dataValueField: "id",
                filter: "contains",
            });

            var room_list = new kendo.data.DataSource({
            transport: {
                read: {
                url: "/reservation/rooms_list",
                dataType: "json"
                }
            }
            });
            $("#rooms").kendoDropDownList({
            dataSource: room_list,
            dataTextField: "room_name",
            dataValueField: "id",
            filter: "contains",
            });
            
            $('span.k-select').click(function(){
                //$('a.k-link.k-nav-prev').hide();
                //$('a.k-link.k-nav-next').hide();
                //3$('a.k-link.k-nav-fast').hide();
                //console.log("POTETAM")
            });
        };
        function scheduler_remove(e) {
            var rid = e.event.id;
            var isOccupied = false;

            if(($user_id!=e.event.user_id&&$user_role!='Admin')){
                e.preventDefault();
            }
            else{       
            $.ajax({
                    url: "/checkReservationFromScheduler/{rid}",
                    type: 'POST',
                    data: 
                    { 
                        rid: rid 
                    },
                    success: function(response)
                    {
                        if(response=='true'){
                            alert("Unable to cancel, please cancel attached fixed asset/s first."); 
                            location.reload(true);
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        e.preventDefault();
                    } 
                });
            }
        }
        function hide(e){
            var buttonsContainer = e.container.find(".k-edit-buttons");
            buttonsContainer.find(".k-scheduler-update").remove();
            buttonsContainer.find(".k-scheduler-delete").remove();
            buttonsContainer.find(".k-scheduler-cancel").remove();
        }
        function dataBound(){
            if(status=="saved"){
            status = "";
            }
            else if(status=="updated"){
                console.log("Updated");
                status = "";
                isUpdating = false;
            }
        };

        var model = kendo.observable({

            equipments : new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/reservation/equipment_list",
                    dataType: "json"
                }
            }
            }),
            rooms : new kendo.data.DataSource({
                transport: {
                    read: {
                    url: "/reservation/rooms_list",
                    dataType: "json"
                    }
                }
            }),
            reserves : new kendo.data.SchedulerDataSource({
                transport: {
                    read: {
                        url:"/reservation/scheduler_list",
                        dataType:"json"
                    },
                    update: {
                        url: "/reservation/update",
                        dataType: "json",
                        type: "POST",
                        complete: function(jqXhr, textStatus) {
                            if (textStatus == 'success') {
                                console.log('Success');
                            } else {
                                alert('Unable to update due to equipment shortage!');
                            }
                        }
                    },
                    create: {
                        url: "/reservation/store",
                        dataType: "json",
                        type:"POST",
                        complete: function(jqXhr, textStatus) {
                            if (textStatus == 'success') {
                                location.reload(true);
                            } 
                            else {                               
                                window.location = 'conflict';
                            }
                        }
                    },
                    destroy: {
                        url: "/reservation/destroy",
                        dataType: "json",
                        type:"POST"
                    },
                        parameterMap: function(options, operation) {
                            if(operation!='read'){
                                if(operation=='create'){
                                    var starts = new Date(options.start);
                                    options.start = kendo.toString(new Date(starts), "yyyy-MM-dd HH:mm:sszz");
                                    var ends = new Date(options.end);
                                    options.end = kendo.toString(new Date(ends), "yyyy-MM-dd HH:mm:sszz");
                                    options.user_id = $user_id;
                                    options.IsAllDay = false;
                                    options.ReservationStatus = "Reserved"; 
                                }   
                                else{
                                    var starts = new Date(options.start);
                                    options.start = kendo.toString(new Date(starts), "yyyy-MM-dd HH:mm:sszz");
                                    var ends = new Date(options.end);
                                    options.end = kendo.toString(new Date(ends), "yyyy-MM-dd HH:mm:sszz");
                                    options.user_id = $user_id; 
                                    options.ReservationStatus = "Reserved"; 
                                }
                            //console.log(options);
                            return options;
                            }
                        }
                    },
                schema: {
                    model: {
                        id: "id",
                        fields:{
                            title: { from: "Title", defaultValue: "Equipment Reservation", validation: { required: true }},
                            start: { type: "date"},
                            end: { type: "date"},
                            fullname: { type: "string", editable:false},
                            department: { type: "string", editable:false},
                            startTimezone: { from: "StartTimezone" },
                            endTimezone: { from: "EndTimezone" },
                            description: { from: "Purpose"},
                            recurrenceId: { from: "RecurrenceID" },
                            recurrenceRule: { from: "RecurrenceRule" },
                            recurrenceException: { from: "RecurrenceException" },
                            roomId: { from: "RoomID", nullable: true },
                            equipments: { from: "Equipments", nullable: true },
                            user_id: {type : "number"},
                            isAllDay: { type: "boolean", from: "IsAllDay"}
                        }
                    }
                }
        }),
        init : function(e){
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;
        $("#scheduler").kendoScheduler({
        date: new Date(date),
        remove:scheduler_remove,
        edit: function(e) {
            if(e.event.id!=0){      
                if(e.event.user_id != $user_id && $user_role != 'Admin'){
                    hide(e);

                }     
                else{
                    isUpdating = true;   
                    console.log("Editing", e.event.id);
                }
            }
            else{                
                $("#fullname").hide();
                $("#department").hide();
                $("#fullnamelbl").hide();
                $("#departmentlbl").hide();
            }
            fetch();
           // e.container.find("input:first").remove();
           // e.container.find("label:first").remove();
        },  
        editable: {
            template: $("#customEditorTemplate").html(),
        },
        dataBound: function(e) { 
           dataBound();           
           /*var scheduler = e.sender;
            $(".k-event").each(function () {
               var uid = $(this).data("uid");
               if (uid) {
                 var event = scheduler.occurrenceByUid(uid);
                 if (event) {
                   $(this).find(".k-event-delete").click(function (clc) {
                     clc.preventDefault();
                     clc.stopPropagation();                         
                     // TODO: replace with nicer modal
                     if (confirm('Do you want to delete ' + event.title + ' ?'))
                     {
                       console.log("test");
                     }
                   });
                 }
               }
            });*/
        },
        save: function(e) { 
            if(isUpdating){
                status="updated";
            }
            else{
                status = "saved";
            }
        },  
        eventTemplate: $("#event-template").html(),
        startTime: new Date(dateTime),
        dataSource: this.reserves,
        height: 800,
        views: [
            { type: "month", 
            selected: true },
            "day"
        ],
        timezone: "Etc/UTC",
        resources: [
                {
                    field: "roomId",
                    name: "Rooms",
                    dataSource: this.rooms,
                    title: "Room"
                },
                {
                    field: "equipments",
                    name: "Equipments",
                    dataSource: this.equipments,
                    multiple: true,
                    title: "Equipments"
                }
            ]
        });

        },
        
        });
        kendo.bind($("#whole"),model);
        model.init();
    }); 
</script>
<style>
.k-calendar .k-nav-fast{
    pointer-events:none;
}
</style>
@endsection