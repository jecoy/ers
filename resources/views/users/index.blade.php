@extends('layouts.firstheader')
 
@section('content')
    <div id="whole">
        <div id="grid" class="fadeIn fourth" data-csrf="{!!csrf_token()!!}">
        </div>
    </div>
    <script id="customEditorTemplate" type="text/x-kendo-template">
    <div class="k-edit-label">
        <label for="status">Role</label>
    </div> 
    <div data-container-for="status" class="k-edit-field">
        <input data-role="dropdownlist"
               data-source="['Admin','User']"
               required="required"
               data-bind="value:role"
               data-change="change"/>
    </div>    
    </script>
    <script>
        $(document).ready(function() {

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        });

        var model = kendo.observable({
            data : new kendo.data.DataSource({
            transport:{
                read:{
                    url:"reservation/users_list",
                    dataType:"json"
                },                
                destroy: {
                    url: "reservation/destroy_user",
                    dataType: "json",
                    type:"POST"
                },
                update: {
                    url: "reservation/update_user",
                    dataType: "json",
                    type: "POST"
                }/*,
                create: {
                    url: "reservation/store_room",
                    dataType: "json",
                    type:"POST"
                }*/
            },
            schema:{
                model:{
                    id:"id",
                    fields:{
                        fullname:
                        {
                            type:"string",
                            validation:{required:true}
                        },
                        role:
                        {
                            type:"string",
                            validation:{required:true}
                        },
                        username:
                        {
                            type:"string",
                            validation:{required:true}
                        },
                        department:
                        {
                            type:"string",
                            validation:{required:true}
                        }
                    }
                }
            },
            pageSize:10
        }),
        init : function(e){
            $("#grid").kendoGrid({
                dataSource: this.data,
                selectable: true,
                height:600,
                editable: {
                    mode: "popup",
                    template: $("#customEditorTemplate").html(),
                },
                filterable: true,
                excel: {
                    fileName: "users.xlsx"
                },
                sortable: {
                            mode: "multiple",
                            allowUnsort: true,
                            showIndexes: true
                        },
                toolbar: [
                    /*{ 
                        name: "create",
                        text: "Add Room" 
                    },*/
                    "search",
                    "excel"],
                columns: [
                    { field: "fullname",title:"Name" },
                    { field: "username",title:"Username" },
                    { field: "role",title:"Role" },
                    { field: "department",title:"Department" },
                            { 
                                command: [
                                   {
                                        name: "edit",
                                            text: { 
                                            edit: "Edit",
                                            update: "Save",
                                            cancel: "Cancel"
                                            }
                                    },
                                    {
                                        name: "destroy",
                                        text: "Delete"
                                    }
                              ],
                                title: "&nbsp;", 
                                width: "250px" 
                            }
                        ],
                pageable:{
                    pageSize:10,
                    refresh:true,
                    buttonCount:5,
                    messages:{
                        display:"{0}-{1}of{2}"
                    }
                }
            });
        },
        });
        kendo.bind($("#whole"),model);
        model.init();
</script>
@endsection