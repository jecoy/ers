<!DOCTYPE html>
<html>
<head>
    <link href="{{asset('bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
    <link href="{{asset('login.css')}}" rel="stylesheet" id="bootstrap-css">
    <script src="{{asset('bootstrap.min.js')}}"></script>
    <script src="{{asset('jquery.min.js')}}"></script>
    <script>
    console.log({{Session::get('user_id')}});
    console.log('{{Session::get('user_role')}}');
    </script>
</head>

<body>
<div class="jumbobox container">
    <div class="wrapper fadeInDown">
        <div class="navbar navbar-dark bg-primary" style="margin-bottom:10px; border-radius: 25px;">
            <img src="{{asset('logo.png')}}" alt="Italian Trulli" class="responsive">
        </div>
          @foreach((array) $message as $x)
                <p style="color:red;">{{ $x }}</p>
          @endforeach
        <div id="formContent" style="padding-top:20px;"> 
            <!-- Login Form -->
            <form method="post" action="{{ url('/login/authenticate') }}">
            {{ csrf_field() }}
            <input type="text" id="username" class="fadeIn second" name="username" placeholder="Username">
            <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
            <input type="submit" class="fadeIn fourth" value="LOGIN">
            </form>
        </div>
        <!-- Remind Passowrd -->
        <div id="formFooter">
        <a class="underlineHover" href="{{ route('index') }}">Welcome to our site!</a>
        </div>
    </div>
</div>

</div>
</body>
</html>