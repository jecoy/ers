@extends('layouts.firstheader')
 
@section('content')
    <div id="whole">
        <div id="grid" class="fadeIn fourth" data-csrf="{!!csrf_token()!!}">
        </div>
    </div>
    <script>
    var x = "search";
    var y = "create";
        $(document).ready(function() {
            console.log({{Session::get('user_id')}});
            console.log('{{Session::get('user_role')}}');
            console.log('{{Session::get('status')}}');

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        });

        var model = kendo.observable({
            data : new kendo.data.DataSource({
            transport:{
                read:{
                    url:"reservation/equipment_list",
                    dataType:"json"
                },
                update: {
                    url: "reservation/update_equipment",
                    dataType: "json",
                    type: "POST"
                },
                destroy: {
                    url: "reservation/destroy_equipment",
                    dataType: "json",
                    type:"POST"
                },
                create: {
                    url: "reservation/store_equipment",
                    dataType: "json",
                    type:"POST"
                }
            },
            schema:{
                model:{
                    id:"id",
                    fields:{
                        equipment_name:
                        {
                            type:"equipment_name",
                            validation:
                            {
                                required:true
                            }
                        },
                        equipment_stocks:
                        {
                            type:"equipment_stocks",
                            validation:
                            {
                                required:true
                            }
                        }
                    }
                }
            },
            pageSize:10
        }),
        init : function(e){
            $("#grid").kendoGrid({
                dataSource: this.data,
                selectable: true,
                height:600,
                editable: "popup",
                filterable: true,
                sortable: {
                            mode: "multiple",
                            allowUnsort: true,
                            showIndexes: true
                        },
                excel: {
                    fileName: "equipments.xlsx"
                },
                toolbar: [
                    { 
                        name: "create",
                        text: "Add Equipments" 
                    },
                    x,
                    "excel"],
                columns: [
                    { 
                        field: "equipment_name",
                        title:"Equipment Name" 
                    }, 
                    { 
                        field: "equipment_stocks",
                        title:"Equipment Stocks" 
                    }, 
                    { 
                command: [
                        {
                            name: "edit",
                            text: { 
                            edit: "Edit",
                            update: "Save",
                            cancel: "Cancel"
                            }
                        }, 
                        {
                            name: "destroy",
                            text: "Delete"
                        }
                    ],
                                title: "&nbsp;", 
                                width: "250px" 
                            }
                        ],
                pageable:{
                    pageSize:10,
                    refresh:true,
                    buttonCount:5,
                    messages:{
                        display:"{0}-{1}of{2}"
                    }
                }
            });
        },
        });
        kendo.bind($("#whole"),model);
        model.init();
</script>
@endsection