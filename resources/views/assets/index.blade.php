@extends('layouts.firstheader') 
@section('content')
    <div id="whole">
        <div id="grid" class="fadeIn fourth" data-csrf="{!!csrf_token()!!}">
        </div>
    </div>

    <script id="customEditorTemplate" type="text/x-kendo-template">
    <div class="k-edit-label">
        <label for="fxa_no">Fixed Asset No</label>
    </div>
    <div data-container-for="fxa_no" class="k-edit-field">
        <input type="text" class="k-textbox" name="fxa_no" title="Fixed Asset No" required="required" autocomplete="disabled" data-bind="value:fxa_no">
    </div>
    <div class="k-edit-label">
        <label for="fxa_no">Description</label>
    </div>
    <div data-container-for="fxa_no" class="k-edit-field">
        <input type="text" class="k-textbox" name="description" title="Fixed Asset Name" required="required" autocomplete="disabled" data-bind="value:description">
    </div>
    <div class="k-edit-label">
        <label for="equipment">Equipment</label>
    </div> 
    <div data-container-for="equipment" class="k-edit-field">
               <select id="equipments" data-bind="value:equipment_id" style="display: none;" name="Equipments" required>
               </select>
    </div>   

    <div class="k-edit-label">
        <label for="status">Status</label>
    </div> 
    <div data-container-for="status" class="k-edit-field">
        <input data-role="dropdownlist"
               data-source="['Available','Borrowed']"
               required="required"
               data-bind="value:status"
               data-change="change"/>
    </div>    
    </script>

    <script>
    var x = "search";
    var y = "create";
    var grid;
    var deleted = 0;
        $(document).ready(function() {
            grid = $("#grid").data("kendoGrid");

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        });

       function fetch(){

        var eq_list = new kendo.data.DataSource({
            transport: {
                read: {
                url: "/reservation/equipment_list",
                dataType: "json"
                }
            }
            });
            $("#equipments").kendoDropDownList({
                dataSource: eq_list,
                dataTextField: "equipment_name",
                dataValueField: "id",
                filter: "contains",
            });

       }

        var model = kendo.observable({
            data : new kendo.data.DataSource({
            transport:{
                read:{
                    url:"/reservation/assets_list",
                    dataType:"json"
                },
                update: {
                    url: "reservation/update_asset",
                    dataType: "json",
                    type: "POST",
                    complete: function(jqXhr, textStatus) {
                        console.log(textStatus);
                        if (textStatus == 'success') {
                            alert("Updated Succesfully");
                            grid.dataSource.read();
                        } 
                        else {                            
                            alert("This Fixed Asset is still borrowed.");
                            grid.dataSource.read();
                        }
                    }
                },
                destroy: {
                    url: "reservation/destroy_asset",
                    dataType: "json",
                    type:"POST",
                    complete: function(jqXhr, textStatus) {
                        console.log(textStatus);
                        if (textStatus == 'success') {
                            //swal("Success", "Asset has been deleted.", "success");
                            alert("Deleted Succesfully");
                            grid.dataSource.read();
                        } 
                        else {                            
                            alert("This Fixed Asset is still borrowed.");
                            grid.dataSource.read();
                        }
                    }
                },
                create: { 
                    url: "reservation/store_asset",
                    dataType: "json",
                    type:"POST"
                }
            },
            schema:{
                model:{
                    id:"id",
                    fields:{
                        fxa_no:
                        {
                            type:"fxa_no",
                            validation:
                            {
                                required:true
                            }
                        },
                        equipment_id:
                        {
                            type:"equipment_id",
                            validation:
                            {
                                required:true
                            }
                        },
                        status:
                        {
                            type:"status",
                            validation:
                            {
                                required:true
                            }
                        }
                    }
                }
            },
            pageSize:10
        }),
        init : function(e){
            $("#grid").kendoGrid({
                dataSource: this.data,
                selectable: true,
                height:600,
                filterable: true,
                edit: function(e) {
                    fetch();
                },
                editable: {
                    mode: "popup",
                    template: $("#customEditorTemplate").html(),
                },
                sortable: {
                            mode: "multiple",
                            allowUnsort: true,
                            showIndexes: true
                        },
                excel: {
                    fileName: "assets.xlsx"
                },
                toolbar: [
                    { 
                        name: "create",
                        text: "Add Fixed Assets" 
                    },
                    x,
                    "excel"],
                columns: [
                    { 
                        field: "fxa_no",
                        title:"Fixed Asset No" 
                    }, 
                    { 
                        field: "description",
                        title:"Fixed Asset Name" 
                    }, 
                    { 
                        field: "equipment_name",
                        title:"Equipment" 
                    }, 
                    { 
                        field: "status",
                        title:"Status" 
                    }, 
                    { 
                command: [
                        {
                            name: "edit",
                            text: { 
                            edit: "Edit",
                            update: "Save",
                            cancel: "Cancel"
                            }
                        }, 
                        {
                            name: "destroy",
                            text: "Delete"
                        }
                    ],
                                title: "&nbsp;", 
                                width: "250px" 
                            }
                        ],
                pageable:{
                    pageSize:10,
                    refresh:true,
                    buttonCount:5,
                    messages:{
                        display:"{0}-{1}of{2}"
                    }
                }
            });
        },
        });
        kendo.bind($("#whole"),model);
        model.init();
</script>
@endsection