@extends('layouts.firstheader')
 
@section('content')
    <div id="whole">
        <div id="grid" class="fadeIn fourth" data-csrf="{!!csrf_token()!!}">
        </div>
    </div>
    <script>
        $(document).ready(function() {

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        });

        var model = kendo.observable({
            data : new kendo.data.DataSource({
            transport:{
                read:{
                    url:"reservation/rooms_list",
                    dataType:"json"
                },
                update: {
                    url: "reservation/update_room",
                    dataType: "json",
                    type: "POST"
                },
                destroy: {
                    url: "reservation/destroy_room",
                    dataType: "json",
                    type:"POST"
                },
                create: {
                    url: "reservation/store_room",
                    dataType: "json",
                    type:"POST"
                }
            },
            schema:{
                model:{
                    id:"id",
                    fields:{
                        room_name:
                        {
                            type:"string",
                            validation:{required:true}
                        }
                    }
                }
            },
            pageSize:10
        }),
        init : function(e){
            $("#grid").kendoGrid({
                dataSource: this.data,
                selectable: true,
                height:600,
                editable: "popup",
                filterable: true,
                sortable: {
                            mode: "multiple",
                            allowUnsort: true,
                            showIndexes: true
                        },
                excel: {
                    fileName: "rooms.xlsx"
                },
                toolbar: [
                    { 
                        name: "create",
                        text: "Add Room" 
                    },
                    "search",
                    "excel"],
                columns: [
                    { field: "room_name",title:"Room Name" },
                            { 
                                command: [
                                    {
                                        name: "edit",
                                            text: { 
                                            edit: "Edit",
                                            update: "Save",
                                            cancel: "Cancel"
                                            }
                                    }, 
                                    {
                                        name: "destroy",
                                        text: "Delete"
                                    }
                              ],
                                title: "&nbsp;", 
                                width: "250px" 
                            }
                        ],
                pageable:{
                    pageSize:10,
                    refresh:true,
                    buttonCount:5,
                    messages:{
                        display:"{0}-{1}of{2}"
                    }
                }
            });
        },
        });
        kendo.bind($("#whole"),model);
        model.init();
</script>
@endsection