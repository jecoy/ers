<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::prefix('reservation')->group(function(){
Route::get('/','ReservationsController@index');
Route::get('list','ReservationsController@list');
});*/


Route::get('/reservation/list', 'ReservationsController@list');
Route::get('/reservation/users_list', 'ReservationsController@users_list');
Route::get('/reservation/scheduler_list', 'ReservationsController@scheduler_list');
Route::get('/borrow_date_list', 'ReservationsController@borrowDateList');
Route::get('/return_date_list', 'ReservationsController@returnDatelist');
Route::get('/reservation/equipment_list', 'ReservationsController@equipment_list');
Route::get('/reservation/rooms_list', 'ReservationsController@rooms_list');
Route::get('/reservation/assets_list', 'ReservationsController@assets_list');

Route::post('/reservation/store', 'ReservationsController@store');
Route::post('/reservation/store_room', 'ReservationsController@store_room');
Route::post('/reservation/store_equipment', 'ReservationsController@store_equipment');
Route::post('/reservation/store_asset', 'ReservationsController@store_asset');


Route::post('/reservation/update', 'ReservationsController@update');
Route::post('/reservation/update_asset', 'ReservationsController@update_asset');
Route::post('/reservation/update_room', 'ReservationsController@update_room');
Route::post('/reservation/update_equipment', 'ReservationsController@update_equipment');
Route::post('/reservation/update_user', 'ReservationsController@update_user');

Route::post('/reservation/destroy', 'ReservationsController@destroy');
Route::post('/reservation/destroy_room', 'ReservationsController@destroy_room');
Route::post('/reservation/destroy_equipment', 'ReservationsController@destroy_equipment');
Route::post('/reservation/destroy_asset', 'ReservationsController@destroy_asset');
Route::post('/reservation/destroy_user', 'ReservationsController@destroy_user');

Route::get('/conflict', 'ReservationsController@conflict')->name('conflict_error');
Route::get('/', 'ReservationsController@index')->name('index');
Route::get('/rooms', 'ReservationsController@rooms_index')->name('room_index');
Route::get('/equipments', 'ReservationsController@equipment_index')->name('equipment_index');
Route::get('/users', 'ReservationsController@users_index')->name('users_index');
Route::get('/assets', 'ReservationsController@assets_index')->name('asset_index');

Route::get('/reservation/{id}', 'ReservationsController@reservation_index')->name('reservation_index');
Route::get('/dates/{id}', 'ReservationsController@dates')->name('dates');
//AJAX
Route::post('/change_status/{rid,remarks}', 'ReservationsController@borrowStatusAjax');
Route::post('/borrowAsset/{rid,fxa_id}', 'ReservationsController@borrowAsset');
Route::post('/cancelAsset/{borrowedFxa_id}', 'ReservationsController@cancelAsset');
Route::post('/cancel_status/{rid}', 'ReservationsController@cancelAjax');
Route::post('/getRemarks/{rid}', 'ReservationsController@getRemarks');
Route::post('/getAssets/{rid}', 'ReservationsController@getAssets');
Route::post('/checkReservationFromScheduler/{rid}', 'ReservationsController@checkReservationFromScheduler');

Route::get('/login', 'ReservationsController@login')->name('login');
Route::get('/logout', 'ReservationsController@logout')->name('logout');
Route::post('/login/authenticate', 'ReservationsController@ldap');
Route::get('/changepass', 'ReservationsController@changepass')->name('changepass');
Route::post('/changepass/authenticate', 'ReservationsController@changepassword');