<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipments extends Model
{
    protected $fillable = [
    'equipment_name','equipment_stocks'    
    ];
    protected $table = "equipments";
    public $timestamps = false;
    /*protected $casts = [
        'Equipments' => 'array',
        'RoomID' => 'array'
    ];*/
    
}