<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Returned extends Model
{
    protected $fillable = [
        'borrow_id','return_date'
    ];
    protected $casts = [
        'Equipments' => 'array',
        'RoomID' => 'array'
    ];
    protected $table = "returned";
    public $timestamps = false;    
}