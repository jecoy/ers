<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Reservations extends Model
{
    protected $fillable = [
        'Title','start','end','Purpose','SharedID','RoomID','Equipments','user_id','ReservationStatus','DateReserved'
    ];

    protected $table = "reservation";
    public $timestamps = false;
    protected $casts = [
        'Equipments' => 'array',
        'RoomID' => 'array'
    ];
}