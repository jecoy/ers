<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remarks extends Model
{
    protected $fillable = [
        'reservation_id','remarks'
    ];
    protected $table = "remarks";
    public $timestamps = false;    
}