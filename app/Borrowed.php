<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrowed extends Model
{
    protected $fillable = [
        'reservation_id','borrow_date'
    ];
    protected $casts = [
        'Equipments' => 'array',
        'RoomID' => 'array'
    ];
    protected $table = "borrowed";
    public $timestamps = false;    
}