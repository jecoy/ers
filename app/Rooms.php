<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rooms extends Model
{
    protected $fillable = [
    'room_name'    
    ];
    protected $table = "rooms";
    public $timestamps = false;    
}