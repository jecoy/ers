<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable = [
        'fullname','username','department','role','last_login_date','isActive','password','email'
    ];

    protected $table = "user";
    public $timestamps = false;
}