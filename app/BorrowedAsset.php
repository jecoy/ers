<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowedAsset extends Model
{
    protected $fillable = [
        'reservation_id','borrow_date','fxa_id'
    ];
    protected $table = "borrowed_asset";
    public $timestamps = false;    
}