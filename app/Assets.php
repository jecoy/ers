<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
    protected $fillable = [
        'fxa_no','status','equipment_id','description'
    ]; 
    protected $table = "assets";
    public $timestamps = false;    
    protected $casts = [
        'equipment_id' => 'array',
    ];
}