<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Reservations as Reservation;
use App\Rooms;
Use App\Equipments;
use App\Borrowed;
use App\BorrowedAsset;
use App\Assets;
use App\Remarks;
use App\Returned;
use App\Users;
use Adldap\Laravel\Facades\Adldap;
use Adldap\AdldapInterface;
use \Crypt;

class ReservationsController extends Controller
{
    //
    public function __construct(BorrowedAsset $borrowedAsset,Assets $asset,Remarks $remarks,Users $users,Reservation $reservation,Equipments $equipments,Rooms $rooms,Borrowed $borrowed,Returned $returned)
    {
        $this->borrowedAsset = $borrowedAsset;
        $this->reservation = $reservation;
        $this->equipments = $equipments;
        $this->rooms = $rooms;
        $this->borrowed = $borrowed;
        $this->returned = $returned;
        $this->users = $users;
        $this->remarks = $remarks;
        $this->asset = $asset;
        //$notif = DB::table('reservation')->get();
        //$notif = Reservation::get();
        //dd($notif);
    }    

    public function logout()
    {   
        session(['status'=>'loggedout']);
        //session(['user_id'=>0]);
        //session(['user_role'=>null]);
        session()->forget('user_role');
        session()->forget('user_id');
        session()->forget('ids');
        session()->save();
        //dd(session('user_id'));die();
        return redirect()->route('index');
    }

    public function index()
    {
        //$users = Adldap::search()->users()->get();
        if(session('user_id')){   
            if(session('user_id')){
                $id = 1;
                session(['id'=>$id]);
                return view('index');
            }
            else{
                return redirect()->route('login');
            }
        }
        else{
            return redirect()->route('login');
        }
    }

    public function changepass()
    {
        if(session('ids')){
            return view('changepass');
        }
        else{
            return redirect()->route('login');
        }
    }
    
    public function changepassword(Request $request)
    {
        //dd(session('user_id'));die();
        $password = $request->password;
        $conpassword = $request->conpassword;
        if($password==$conpassword&&($password!=''||$conpassword!='')){
            $id = session('ids');
            $user = Users::find($id);
            $user->password = Crypt::encrypt($password);
            $user->save();
            session(['user_id'=>$user->id]);
            session(['user_role'=>$user->role]);
            return redirect()->route('index');
        }
        else{
            return redirect()->route('changepass');
        }
        //dd($passwords);die();
        return redirect()->route('index');
    }

    public function login()
    {
        $data = [];
        $data['message'] = '';
        session(['status'=>'loggedin']);
        if(!session('user_id')){
            return view('login',$data);
        }
        else{
            return redirect()->route('index');
        }
    }

    public function ldap(Request $request)
    {
        $data = [];
        $data['message'] = 'Invalid credentials.';
        $today = Carbon::now('Asia/Manila')->toDayDateTimeString();
        $username = $request->username;
        $password = $request->password;
        $user_info = Users::where('username', $username)->get();
        $count = count($user_info);
        if($count > 0){         
            session(['ids'=>$user_info[0]->id]);
            if($user_info[0]->password!=''){
                $user_password = Crypt::decrypt($user_info[0]->password);
                if($password==$user_password){                      
                    session(['user_id'=>$user_info[0]->id]);
                    session(['user_role'=>$user_info[0]->role]);
                    $user = Users::find($user_info[0]->id);
                    $user->last_login_date = $today;
                    $user->save();
                    return redirect()->route('index');
                }
                else{
                    return view('login',$data);
                }
            }
            else{
                return redirect()->route('changepass');
            }            
        }
        else{
            $user = Adldap::search()->where('samaccountname', '=', $username)->get();
            $rowNum = count($user);
            if($rowNum>0){
                $users[] = $user;
                $insertUser['fullname'] = $users[0][0]->cn[0];
                $insertUser['username'] = $users[0][0]->samaccountname[0];
                $insertUser['department'] = $users[0][0]->department[0];
                $insertUser['role'] = 'User';
                $insertUser['last_login_date'] = $today;
                $insertUser['isActive'] = true;
                $insertUser['password'] = '';
                $insertUser['email'] = $users[0][0]->mail[0];
                $insertedUser = Users::create($insertUser);
                //dd($insertedUser->id);die();
                session(['ids'=>$insertedUser->id]);
                return redirect()->action('ReservationsController@changepass');
            }
            else{
                return view('login',$data);
            }
        }
    }

    public function dates($id)
    {
        session(['dates_id'=>$id]);
        if(session('user_id')){
            return view('reservations.dates');
        }
        else{
            return redirect()->route('login');
        }
    }

    public function reservation_index($id)
    {   
        session(['id'=>$id]);
        if(session('user_id')){
            return view('reservations.index');
        }
        else{
            return redirect()->route('login');
        }
    }

    public function conflict()
    {
        return view('conflict');
    }

    public function rooms_index()
    {
        if(session('user_id')){
            return view('rooms.index');
        }
        else{
            return redirect()->route('login');
        }
    }

    public function assets_index()
    {
        if(session('user_id')){
            session(['id'=>0]);
            return view('assets.index');
        }
        else{
            return redirect()->route('login');
        }
    }

    public function equipment_index()
    {
        if(session('user_id')){
            return view('equipments.index');
        }
        else{
            return redirect()->route('login');
        }
    }

    public function users_index()
    {
        if(session('user_id')){
            return view('users.index');
        }
        else{
            return redirect()->route('login');
        }
    }

    public function list()
    {   
        if(session('user_id')){
        $sesh_id = session('id');

        if($sesh_id==1){
            $filter = "Reserved";
        }
        else if($sesh_id==2){
            $filter = "Borrowed";
        }
        else if($sesh_id==3){
            $filter = "Returned";
        }
        else{
            $filter = "Pending";
        }

        if(session('user_role')!='Admin'){      
            $reservation = Reservation::select('reservation.*', 'user.fullname','user.department')
                            ->join('user','reservation.user_id','=','user.id')
                            ->where(['ReservationStatus' =>  $filter,
                                     'user_id' => session('user_id')])
                            ->orderByDesc('id')
                            ->get(); 
        }
        else{
            $reservation = Reservation::select('reservation.*', 'user.fullname','user.department')
                            ->join('user','reservation.user_id','=','user.id')
                            ->where(['ReservationStatus' =>  $filter])
                            ->orderByDesc('id')
                            ->get();                  
        }                                                   
        
        return response()->json($reservation);
        }
    }

    public function scheduler_list()
    {   
        if(session('user_id')){
        $sesh_id = session('id');
        $reservation = Reservation::select('reservation.*', 'user.fullname','user.department')
                                    ->join('user','reservation.user_id','=','user.id')
                                    ->where('reservation.ReservationStatus','=', 'Reserved')
        //                            ->orWhere('ReservationStatus','=','Borrowed')
                                    ->get(); 
                                                                                           
        return response()->json($reservation);
        }
    }
          
    public function borrowDateList()
    {   
        if(session('user_id')){
            if(session('user_role')=='Admin'){
                $borrowed = Borrowed::select('borrowed.*', 'reservation.*')
                ->join('reservation', 'borrowed.reservation_id','=','reservation.id')
                ->orderByDesc('borrowed.reservation_id')
                ->get();
            }
            else{
                $borrowed = Borrowed::select('borrowed.*', 'reservation.*')
                ->join('reservation', 'borrowed.reservation_id','=','reservation.id')
                ->where(['reservation.user_id' => session('user_id')])
                ->orderByDesc('borrowed.reservation_id')
                ->get();
            }            
            return response()->json($borrowed);
        }
    }

    public function returnDateList()
    {       
        if(session('user_id')){
            if(session('user_role')=='Admin'){
                $return = Returned::join('borrowed', 'returned.borrow_id','=','borrowed.id')
                ->join('reservation', 'borrowed.reservation_id', '=', 'reservation.id')
                ->orderByDesc('borrowed.reservation_id')
                ->get(); 
            }
            else{
                $return = Returned::join('borrowed', 'returned.borrow_id','=','borrowed.id')
                ->join('reservation', 'borrowed.reservation_id', '=', 'reservation.id')
                ->where(['reservation.user_id' => session('user_id')])
                ->orderByDesc('borrowed.reservation_id')
                ->get(); 
            }
            return response()->json($return);
        }
    }

    public function equipment_list()
    {
        if(session('user_id')){
            $equipments = DB::table('equipments')->get();
            //Equipments::all();
            return response()->json($equipments);
        }
        else{
            return redirect()->route('login');
        }
    }

    public function rooms_list()
    {
        if(session('user_id')){
        $rooms = Rooms::all();
        return response()->json($rooms);
        }
    }

    public function users_list()
    {
        if(session('user_id')){
        $users = Users::all();
        return response()->json($users);
        }
    }
    
    public function assets_list()
    {
        if(session('user_id')){
            $id = session('id');
            if($id==0){
                $assets = Assets::select('assets.*', 'equipments.equipment_name')
                                  ->join('equipments', 'assets.equipment_id','=','equipments.id')
                                  ->where(['status' => 'Available'])
                                  ->orderByDesc('assets.equipment_id')
                                  ->get();
            }
            else{
                $assets = Assets::select("id", DB::raw("CONCAT(assets.fxa_no,' - ',assets.description) as desc"))
                                  ->where(['status'=>'Available'])->get();
            }
            return response()->json($assets);
        }
    }
    public function store(Request $request)
    {        
        if(session('user_id')){               
        $error = 0;
        $validator = 0;
        $noOfDaysindex = 0;

        //getting the number of equipments requested    
        $equipments_length = count($request->Equipments); 
           
        //passing the request to new editable request for future storing purposes
        $newRequest[0] = $request->all();
        /**in this loop we made a stocks_counter array and existing_counter array indexed 
         * by its equipments id to store the current stock of each requested equipments    
         * */
        for($index = 0 ; $index < $equipments_length ; $index++){
            $id = $request->Equipments[$index]['id'];
            $equipments = Equipments::find($id);
            $stocks_counter[$id] = $equipments->equipment_stocks;
            $existing_counter[$id] = 0;

        }
        /**
         * we fetch all the existing reservations that are still reserved and borrowed
         */
        $fetchData = Reservation::where('ReservationStatus','=', 'Reserved')
                                ->orWhere('ReservationStatus','=','Borrowed')
                                ->orWhere('ReservationStatus','=','Pending')
                                ->orderByDesc('id')
                                ->get();
                                
        $startTime = Carbon::parse(request('start'));
        $endTime = Carbon::parse(request('end'));
        //subtracting between the number of days from the starting of reservation until the end
        $noOfDays = $startTime->diffInDays($endTime, false);
        //this condition assigns shared id to those series reservations
        if($noOfDays>0){
            $id = DB::table('reservation')
            ->max('SharedID'); 
            if($id==null){
                $id++;
            }      
            $newRequest[0]['SharedID'] = $id++;
        }        

        $origStart = $startTime;
        $origEnd = $endTime;

        $newRequest[0]['DateReserved'] = Carbon::now();

        /**
         * in this loop we will check if the requested current reservation does not conflict other existing reservation
         */
        while($noOfDaysindex <= $noOfDays){
            /**
             * in this section we assign a new starting and end reservation , 
             * and in order to do that we add number of days for the requested reservation and 
             * subtracted the original end date with the difference of the length of reservation days
             *  and the current number iteration made by the while loop represented by '$noOfDaysindex'
             */
            $newStart = $origStart->addDays($noOfDaysindex); 
            $newEnd = $origEnd->subDays($noOfDays-$noOfDaysindex);

            //assign the new start date and new end date to the new editable request
            $newRequest[0]["start"] = $newStart;
            $newRequest[0]["end"] = $newEnd;
            
            /**
             * this will loop the fetched reservations
             */
            foreach ($fetchData as $fetch) {

                $end = Carbon::parse($fetch->end);
                $start = Carbon::parse($fetch->start);
                /**
                 * this condition will check if the current requested reservation and the current
                 * fetched reservation has the same time and date or is conflicted with each other
                */
                if(($end > $newStart && $start < $newEnd) || ($end == $newEnd && $start == $newStart)){
                   //it will then pass the current fetched reservations equipments 
                   $equipments = $fetch->Equipments;
                    /**
                     * this will loop the requested equipments
                     */
                   for($index = 0 ; $index < $equipments_length; $index++){
                    //passing of the current looped requested equipment
                    $p_id = $request->Equipments[$index]['id'];
                        /**
                         * this will loop the current fetched equipments
                         */
                        for($index2 = 0 ; $index2 < count($equipments); $index2++){ 
                            /**
                             * this condition will check if the current looped requested equipment
                             * is the same with the current looped fetched equipment
                             */
                        
                            if($p_id == $equipments[$index2]['id']){
                                /**
                                 * the value of the existing counter array which has the index of the current requested equipment id
                                 * will now be then added by 1
                                 *  */    
                                $existing_counter[$p_id] = $existing_counter[$p_id] + 1;

                            }
                        }
                   }                    
                }
            }

            /**
             * this will loop the requested equipments
             */
            for($index = 0 ; $index < $equipments_length ; $index++){

                $id = $request->Equipments[$index]['id'];
                /**
                 *  it will then check if the number of stocks of the current looped requested equipment 
                 *  is greater than the number of the current looped existing equipment
                 */
               
                if($stocks_counter[$id]>$existing_counter[$id]){
                    //it will the add the validator value by 1
                    $validator++;

                }
            }
            /**
             * in this condition if the validator's value is the same with the equipments length it means
             * that there are no problems with any of the stocks of the requested equipments
             * else it will add up the number of error counter and insert a message in the message array regarding the conflict
             */
            if($validator == $equipments_length){
                $reservation = Reservation::create($newRequest[0]);
            }

            else{                
                $error++;
                $message[] = '• Failed to reserve from: '.$newRequest[0]["start"]. ' to '.$newRequest[0]["end"].' due to shortage of equipment/s.';
            }
            //this loop will then refresh the existing_counter arrays values back to 0
            for($index = 0 ; $index < $equipments_length ; $index++){
                $id = $request->Equipments[$index]['id'];
                $existing_counter[$id] = 0;
            }
            //set the used variables to their default values.
            $validator = 0;
            $origStart = Carbon::parse(request('start'));
            $origEnd = Carbon::parse(request('end'));
            //add 1 to the number of days as this while loop iterates 
            $noOfDaysindex++;
        }
        //if there are any conflicts return with error
        if($error > 0 ){           
            $collected_message = collect($message);
            session(['message'=>$collected_message]);
            return response()->json($collected_message)->setStatusCode(404,"ERROR");
        }
        else{
            $reservation = Reservation::all();
            return response()->json($reservation);    
        }

        }
    } 

    public function store_room(Request $request)
    {
        if(session('user_id')){
            $rooms = Rooms::create($request->all());
            return response()->json($rooms);      
        }  
    }

    public function store_equipment(Request $request)
    {
        if(session('user_id')){
            $equipments = Equipments::create($request->all());
            return response()->json($equipments);    
        }    
    }

    public function store_asset(Request $request)
    {
        if(session('user_id')){      
            $newRequest[0] = $request->all();
            $newRequest[0]['equipment_id'] = (int)$newRequest[0]['equipment_id']['id'];
            $asset = Assets::create($newRequest[0]);
            $returned_asset = Assets::select('assets.*', 'equipments.equipment_name')
                            ->join('equipments', 'assets.equipment_id','=','equipments.id')
                            ->where(['assets.id' => $asset->id])
                            ->orderByDesc('assets.equipment_id')
                            ->get();
            return response()->json($returned_asset);    
        }    
    }

    public function update(Request $request)
    {
        if(session('user_id')){
        $validator = 0;

        //getting the number of equipments requested    
        $equipments_length = count($request->Equipments); 
        /**in this loop we made a stocks_counter array and existing_counter array indexed 
         * by its equipments id to store the current stock of each requested equipments    
         * */    
        for($index = 0 ; $index < $equipments_length ; $index++){
            $id = $request->Equipments[$index]['id'];
            $equipments = Equipments::find($id);
            $stocks_counter[$id] = $equipments->equipment_stocks;
            $existing_counter[$id] = 0;
        }

        $startTime = Carbon::parse(request('start'));
        $endTime = Carbon::parse(request('end'));
        /**
         * we fetch all the existing reservations that are still reserved and borrowed
         */
        $fetchData = Reservation::where('ReservationStatus','=', 'Reserved')
                                ->orWhere('ReservationStatus','=','Borrowed')
                                ->orWhere('ReservationStatus','=','Pending')
                                ->orderByDesc('id')
                                ->get();
        /**
         * this will loop the fetched reservations
         */                        
        foreach ($fetchData as $fetch){

            $end = Carbon::parse($fetch->end);
            $start = Carbon::parse($fetch->start);
            /**
             * this condition will check if the current requested reservation and the current
             * fetched reservation has the same time and date or is conflicted with each other
            */
            if(($end > $startTime && $start < $endTime) || ($end == $endTime && $start == $startTime)){             
                //it will then pass the current fetched reservations equipments 
                $equipments = $fetch->Equipments;
                /**
                * this will loop the requested equipments
                */
                for($index = 0 ; $index < $equipments_length; $index++){
                    //passing of the current looped requested equipment
                    $p_id = $request->Equipments[$index]['id'];
                    /**
                    * this will loop the current fetched equipments
                    */
                    for($index2 = 0 ; $index2 < count($equipments); $index2++){ 
                        /**
                        * this condition will check if the current looped requested equipment
                        * is the same with the current looped fetched equipment
                        */
                        if($p_id == $equipments[$index2]['id']){
                            /**
                            * this condition will check if the current looped requested reservation
                            * is the same with the current looped fetched reservation
                            */
                            if($fetch->id != $request->id){
                                $existing_counter[$p_id] = $existing_counter[$p_id] + 1;
                            }
                        }
                    }
                }         
            }    
        }
        /**
        * this will loop the requested equipments
        */
        for($index = 0 ; $index < $equipments_length ; $index++){
            $id = $request->Equipments[$index]['id'];
            /**
            *  it will then check if the number of stocks of the current looped requested equipment 
            *  is greater than the number of the current looped existing equipment
            */
            if($stocks_counter[$id]>$existing_counter[$id]){
                $validator++;
            }
        }
        /**
        * in this condition if the validator's value is the same with the equipments length it means
        * that there are no problems with any of the stocks of the requested equipments
        * else it will add up the number of error counter and insert a message in the message array regarding the conflict
        */
        if($validator == $equipments_length){
            $reservation_id = request('id');
            $reservation = Reservation::find($reservation_id);
            $reservation->ReservationStatus = request('ReservationStatus');
            $reservation->Title = request('Title');
            $reservation->start = request('start');
            $reservation->end = request('end');
            $reservation->Purpose = request('Purpose');
            $reservation->SharedID = request('SharedID');
            $reservation->RoomID = request('RoomID');
            $reservation->Equipments = request('Equipments');
            $reservation->DateReserved = request('DateReserved');
            $reservation->save();
            $message['CODE:200'] = 'Success';
            $reservation = Reservation::select('reservation.*', 'user.fullname','user.department')
                                    ->join('user','reservation.user_id','=','user.id')
                                    ->where('reservation.id','=', $reservation_id)
                                    ->get(); 
            return response()->json($reservation);
        }
        else{
            $message['Purpose'] = 'Failed';
            return response()->json($message)->setStatusCode(404,"ERROR");
        }
        for($index = 0 ; $index < $equipments_length ; $index++){
            $id = $request->Equipments[$index]['id'];
            $existing_counter[$id] = 0;
        }        
        }
    }

    public function update_room()
    {
        if(session('user_id')){
            $room_id = request('id');
            $rooms = Rooms::find($room_id);
            $rooms->room_name = request('room_name');
            $rooms->save();

            return response()->json($rooms);
        }
    }
    
    public function update_equipment()
    {        
        if(session('user_id')){
            $equipment_id = request('id');
            $equipments = Equipments::find($equipment_id);
            $equipments->equipment_name = request('equipment_name');
            $equipments->equipment_stocks = request('equipment_stocks');
            $equipments->save();

            return response()->json($equipments);
        }
    }

    public function update_asset()
    {        
        if(session('user_id')){
            $asset_id = request('id');
            $asset = Assets::find($asset_id);
            $assetChecker = BorrowedAsset::where(['fxa_id'=>$asset->id])->get();
            if(count($assetChecker)>0){
                return response()->json($assetChecker)->setStatusCode(404,"ERROR");
            }
            else{
                $asset->fxa_no = request('fxa_no');
                $asset->equipment_id = (int)request('equipment_id');
                $asset->description = request('description');
                $asset->status = request('status');
                $asset->save();
    
                return response()->json($asset);
            }
        }
    }

    public function update_user()
    {
        if(session('user_id')){
            $user_id = request('id');
            $users = Users::find($user_id);
            $users->role = request('role');
            $users->save();

            return response()->json($users);
        }
    }
    
    public function destroy(Request $request)
    {
        if(session('user_id')){
            $reservation_id = request('id');
            /*$reservation = Reservation::find($reservation_id);
            $reservation->delete();*/            
            if(!$this->reservationHasAsset($reservation_id)){
                $reservation = Reservation::find($reservation_id);
                $reservation->ReservationStatus = "Cancelled";
                $reservation->save();
        
                return response()->json($reservation);        
            }  
            else{
                return response()->json("ERROR");        
            }  
        }
    }

    public function destroy_room()
    {
        if(session('user_id')){
            $room_id = request('id');
            $rooms = Rooms::find($room_id);
            $rooms->delete();
    
            return response()->json($rooms);
        }
    }

    public function destroy_equipment()
    {
        if(session('user_id')){
            $equipment_id = request('id');
            $equipments = Equipments::find($equipment_id);
            $equipments->delete();
                
            return response()->json($equipments);
        }
    }

    public function destroy_user()
    {
        if(session('user_id')){
            $user_id = request('id');
            $users = Users::find($user_id);
            $users->delete();

            return response()->json($users);
        }
    }

    public function destroy_asset()
    {
        if(session('user_id')){
            $asset_id = request('id');
            $asset = Assets::find($asset_id);
            $asset->delete();

            return response()->json($asset);

        }
    }

    public function borrowStatusAjax()
    {        
        if(session('user_id')){
            $reservation_id = $_POST['rid'];
            $remarks = $_POST['remarks'];

            $today = Carbon::now('Asia/Manila')->toDayDateTimeString();
            $id = session('id');

        if($id==1 || $id==3){
            $newStatus = "Borrowed";
            if($id==1){
                $borrowed = new Borrowed;
                $borrowed->borrow_date = $today;
                $borrowed->reservation_id = $reservation_id; 
                $borrowed->save();

                $_remarks = new Remarks;
                $_remarks->remarks = $remarks;
                $_remarks->reservation_id = $reservation_id; 
                $_remarks->save();
            }

            if($id==3){
                $_remarks = $this->updateRemarks($reservation_id,$remarks);

                return response()->json($_remarks);
            }
        }

        else if($id==2 || $id==4){

            if(session('user_role')=='Admin'){
                $_borrowedChecker = new BorrowedAsset;
                $_borrowedChecker = BorrowedAsset::where(['reservation_id' =>  $reservation_id])->get(); 
                if(count($_borrowedChecker)==0){
                    $newStatus = "Returned";
                    $this->createReturnDate($reservation_id,$today);
                    $this->updateRemarks($reservation_id,$remarks);
                }               
                else{
                    return response()->json($_borrowedChecker)->setStatusCode(404,"ERROR");
                }
            }
            else{
                $newStatus = "Pending";
            } 

        }

        $this->changeStats($reservation_id,$newStatus);

        return response()->json($remarks);

        }        
    }

    public function createReturnDate($reservation_id,$today)
    {
        if(session('user_id')){
            $borrowed = new Borrowed;
            $borrowed = Borrowed::where('reservation_id', $reservation_id)->first();

            $returned = new Returned;
            $returned->return_date = $today;
            $returned->borrow_id = $borrowed->id;
            $returned->save();    
        }
    }

    public function updateRemarks($reservation_id,$remarks)
    {
        if(session('user_id')){
            $_remarks = new Remarks;
            $_remarks = Remarks::where('reservation_id', $reservation_id)->first();
            $_remarks->remarks = $remarks;
            $_remarks->save();
        }
    } 

    public function cancelAjax()
    {
        if(session('user_id')){
            $reservation_id = $_POST['rid'];

            $borrowed = new Borrowed;
            $_remarks = new Remarks;
            $returned = new Returned;

            $borrowed = Borrowed::where('reservation_id', $reservation_id)->first();
            $_remarks = Remarks::where('reservation_id', $reservation_id)->first();

            $id = session('id');

        if($id==1){
            if(!$this->reservationHasAsset($reservation_id)){
                $this->changeStats($reservation_id,"Cancelled");
            }
            else{
                $message = 'ERROR';
                return response()->json($message)->setStatusCode(404,"ERROR");
            }
        }
        else if($id==2){
            $this->changeStats($reservation_id,"Reserved");
        }
        else if($id==3||$id==4){
            $this->changeStats($reservation_id,"Borrowed");
        }

        if($id==3){
           $returned = Returned::where('borrow_id', $borrowed->id)->first(); 
           $returned->delete();      
        }

        else if($id==2){
            $borrowed->delete();
            $_remarks->delete();             
        }

        return response()->json($id);
        }
    }
    
    public function reservationHasAsset($reservation_id)
    {
        $borrowedAsset = new BorrowedAsset;
        $borrowedAsset = BorrowedAsset::where('reservation_id', $reservation_id)
                                        ->get(); 
        if(count($borrowedAsset)>0){
            return true;
        }               
        else{
            return false;
        }                 
    }

    public function checkReservationFromScheduler()
    {
        $reservation_id = $_POST['rid'];
        $borrowedAsset = new BorrowedAsset;
        $borrowedAsset = BorrowedAsset::where('reservation_id', $reservation_id)
                                        ->get(); 
        if(count($borrowedAsset)>0){
            return response()->json('true');
        }               
        else{
            return response()->json('false');
        }                 
    }


    public function changeStats($reservation_id,$newStatus)
    {
        if(session('user_id')){
            $reservation = new Reservation;
            $reservation = Reservation::find($reservation_id);
            $reservation->ReservationStatus = $newStatus;
            $reservation->save();
        }
    }

    public function getRemarks()
    {
        if(session('user_id')){
        $reservation_id = $_POST['rid'];
        $_remarks = new Remarks;
        $_remarks = Remarks::where('reservation_id', $reservation_id)->first();
        $remarks = $_remarks->remarks;
        return $remarks;
        }
        
    }

    public function getAssets()
    {
        if(session('user_id')){
            $id = session('id');
            $reservation_id = $_POST['rid'];
            $borrowedAsset = new BorrowedAsset;
            $borrowedAsset = BorrowedAsset::select('borrowed_asset.*', 'assets.fxa_no','assets.description')
                                            ->join('assets', 'borrowed_asset.fxa_id','=','assets.id')
                                            ->where('reservation_id', $reservation_id)
                                            ->orderBy('borrowed_asset.id')
                                            ->get(); 
            $borrowedAssetCounter = count($borrowedAsset);                                
            $html ='';
            $newHtml = '<tr><th>FIX ASSET</th><th>ACTION</th></tr>';
            foreach($borrowedAsset as $row)
            {
                if($id==1){
                    $html ='<tr>
                        <td>'.$row->fxa_no.' - '.$row->description.'</td>'. 
                        '<td><button class="btn btn-danger" id="cancelFxa" value="'.$row->id.'">CANCEL</button></td>' .
                        '</tr>';
                    $newHtml = $newHtml.$html;
                }
                else{
                    $html ='<tr>
                        <td>'.$row->fxa_no.' - '.$row->description.'</td>'. 
                        '<td><button class="btn btn-danger" id="cancelFxa" value="'.$row->id.'">RETURNED</button></td>' .
                        '</tr>';
                    $newHtml = $newHtml.$html;
                }
            }
    
            return $newHtml;
        }
    }

    public function borrowAsset()
    {
        if(session('user_id')){
            $today = Carbon::now('Asia/Manila')->toDayDateTimeString();
            $reservation_id = $_POST['rid']; 
            $fxa_id = $_POST['fxa_id']; 
            $_borrowedChecker = new BorrowedAsset;
            $_borrowedChecker = BorrowedAsset::where(['reservation_id' =>  $reservation_id,
                                               'fxa_id' => $fxa_id])
                                               ->get(); 
            if(count($_borrowedChecker) > 0){
                return response()->json('404');
            }
            else{
                $asset = Assets::where('id', $fxa_id)->first();
                $asset->status = 'Borrowed';
                $asset->save();
    
                $borrowed_asset = new BorrowedAsset;
                $borrowed_asset->reservation_id = $reservation_id;
                $borrowed_asset->borrow_date = $today; 
                $borrowed_asset->fxa_id = $fxa_id; 
                $borrowed_asset->save();          
    
                return $borrowed_asset;
            }
        }
    }

    public function cancelAsset()
    {
        if(session('user_id')){
            $borrowedFxa_id = $_POST['borrowedFxa_id']; 
            $borrowed = BorrowedAsset::find($borrowedFxa_id);
            $fxa_id = $borrowed->fxa_id;
            $borrowed->delete();

            $asset = Assets::where('id', $fxa_id)->first();
            $asset->status = 'Available';
            $asset->save();

    
            return response()->json($borrowed);
            }
    }


}
